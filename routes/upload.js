var express = require('express');
var router = express.Router();
var multer = require('multer');
var upload = multer();

/* GET users listing. */
router.get('/', upload.fields([]) ,function(req, res, next) {

    console.log("fonction upload du serveur");

    if (req.method === 'GET')
        return res.json({'status': 'GET not allowed'});

    console.log("juste un test");

    var filename = req.file('file')._files[0].stream.filename;
    //to extract the extension
    var positionDot = filename.lastIndexOf(".");
    var extension = filename.substring(positionDot+1,filename.length);

    if(extension!=("zip"||"docx"||"pdf"))
        return res.send({error: 600});
    var index;
    var uploadFile = req.file('file');
    uploadFile.upload({
        dirname: '../../assets/app/files',
        // don't allow the total upload size to exceed ~10MB
        maxBytes: 10000000,
        saveAs: function (__newFileStream,cb) { cb(null, filename); }

    }, function onUploadComplete(err, files) {
        if (err)

            return res.send({error: 600}); //res.negotiate(err);


        if (files.length === 0) {
            return res.badRequest('no file was uploaded');
        }

        console.log("file fd : " + files[0].fd);

        console.log("req.session.user.id: " + req.session.user.id);
        var test=req.session.user.id;
        Project.find({owner:test})
            .exec(function findP(err,project){
                if(!err){
                    console.log("project.length: " + project.length);
                    index=project[0].idHf;
                    // just to test
                    var nameFile = files[0].fd + '';
                    var url = nameFile.substring(nameFile.lastIndexOf('/') + 1);
                    console.log("index: " + index);
                    Project.update({idHf: index}, {filePath: url}).exec(function createCB(err, projects) {
                        if (!err)
                            return res.send({status: 200, project: projects[0]});
                        else
                            return res.send(err);
                    });

                }
                else{
                    console.log("il y a eu des erreurs upload");
                    res.send(err);

                }
            })
        /*
      var nameFile = files[0].fd + '';
      var url = nameFile.substring(nameFile.lastIndexOf('/') + 1);
      console.log("index: " + index);
      Project.update({idHf: index}, {filePath: url}).exec(function createCB(err, projects) {
        if (!err)
          return res.send({status: 200, project: projects[0]});
        else
          return res.send(err);
      }); */


    });


    // res.send('respond with a resource');
});

module.exports = router;
